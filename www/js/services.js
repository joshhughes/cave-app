angular.module('starter.services', [])

.service('AuthService', function (){

  this.saveUser = function(user){
    console.log(user);
    window.localStorage.cave_user = JSON.stringify(user);
  };

  this.getUser = function(){

    return (window.localStorage.cave_user) ?
      JSON.parse(window.localStorage.cave_user) : null;
  };

  this.removeUser = function() {
    if (typeof window.localStorage.cave_user != 'undefined') {
      window.localStorage.removeItem('cave_user');
    }
  }

  this.checkUserAL = function() {
    window.localStorage.cave_user
  }

})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Cave Leaders',
    lastText: 'Tonight is going to be great!',
    face: 'img/thecavelogo.jpg'
  }, {
    id: 1,
    name: 'Kristen Seay',
    lastText: 'Starbucks is my life',
    face: 'img/kristen.jpg'
  }, {
    id: 2,
    name: 'Sawyer Barnes',
    lastText: 'I should buy a boat',
    face: 'img/sawyer.jpg'
  }, {
    id: 3,
    name: 'Pastor Josh Jones',
    lastText: 'I forgot the bacon...',
    face: 'img/pj.jpg'
  }, {
    id: 4,
    name: 'Jesse Bond',
    lastText: 'Im running late',
    face: 'img/jesse.jpg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
