// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers', 'starter.services'])

    // Platforms
.config(function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.tabs.style('standard');
})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  $httpProvider.interceptors.push( [ function() {
    return {
      'request': function( config ) {
        config.headers = config.headers || {};
        //Assume that you store the token in a cookie.

        var stored_user = window.localStorage['cave_user'];
        if(typeof(stored_user) != "undefined"){
          var user = JSON.parse(stored_user);
        } else {
          var user = {};
        }

        //If the cookie has the CurrentUser and the token
        //add the Authorization header in each request
        if ( user && user.user_token ) {
          config.headers.Authorization = 'Bearer ' + user.user_token;
        }
        return config;
      }
    };
  } ] );
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'MainCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    cache: false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

      .state('tab.create', {
      url: '/dash/create',
      views: {
        'tab-dash': {
          templateUrl: 'templates/create.html',
          controller: 'CreateCtrl'
        }
      }
    })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

    .state('tab.hotspots', {
      url: '/hotspots',
      views: {
        'tab-hotspots': {
          templateUrl: 'templates/tab-hotspots.html',
          controller: 'HotspotsCtrl'
        }
      }
    })

    .state('tab.hotspotdetail', {
      url: '/hotspots/:name?lat?lng?desc?date?attending',
      views: {
        'tab-hotspots': {
          templateUrl: 'templates/spot.html',
          controller: 'HotspotsDetailCtrl'
        }
      }
    })

  .state('tab.events', {
    url: '/events',
    views: {
      'tab-events': {
        templateUrl: 'templates/tab-events.html',
        controller: 'EventsCtrl'
      }
    }
  })

  .state('tab.account', {
    url: '/account',
    cache: false,
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  //AUTH ROUTES

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'
  })

  .state('createaccount', {
    url: "/create-account",
    templateUrl: "templates/create-account.html",
    controller: 'CreateAccountCtrl'
  })
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
