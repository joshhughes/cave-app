angular.module('starter.controllers', [])

.controller('MainCtrl', function($scope, $state, $ionicModal, AuthService, $http){
  
  $ionicModal.fromTemplateUrl('templates/create.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
    
  })

// AUTH CONTROLLERS
.controller('LoginCtrl', function($scope, $state, $ionicModal, AuthService, $http){
  $scope.error = false;
  $scope.errormess = "";

  $scope.loading = false;


  $scope.isLoggedIn = function() {

    $scope.loading = true;

    $http.get('http://joshhughes.co/wp-json/wp/v2/posts'
      ).then(function successCallback(response) {
      if (AuthService.getUser() != null && AuthService.getUser() != 'undefined') {
        $scope.loading = false;
      $state.go('tab.dash');
    } else {
      $scope.loading = false;
    }

  }, function errorCallback(response) {
      console.log('Error!');
      $scope.loading = false;
      if (response.data.code == 'jwt_auth_invalid_token') {
        AuthService.removeUser();
        $scope.errormess = "Your session has expired, please sign in again.";
        $scope.error = true;
      } else {
        $scope.loading = false;
      }
  });

    
  }

  $scope.signUp = function() {
    $state.go('createaccount');
  }

  $scope.doLogIn = function(user){
    $scope.loading = true;

    $scope.error = false;
    $scope.errormess = "";

    var info = {
         username: user.email,
        password: user.password
    }

    console.log("doing log in");
    $http.post('http://joshhughes.co/wp-json/jwt-auth/v1/token', info
      ).then(function successCallback(response) {
      console.log('Success!');
      console.log(response);
    
      var user_info = {
        user_token:  response.data.token,
        user_name:response.data.user_display_name,
        user_email: response.data.user_email,
        user_id: response.data.user_id,
        user_firstname: response.data.user_firstname,
        user_roles: response.data.user_roles
      };

      AuthService.saveUser(user_info);
      $state.go('tab.dash');

  }, function errorCallback(response) {
      $scope.loading = false;
      console.log('Error!');
      console.log(response);
      $scope.errormess = response.data.message;
      $scope.error = true;
  });
    
  };

  $scope.isLoggedIn();

})

.controller('CreateAccountCtrl', function($scope, $state, $ionicModal, AuthService, $http){
  
  $scope.error = "";

  $scope.createAccount = function(user){
    $scope.error = "";
    console.log("Checking for vailid information.");
    if (user.password !== user.passwordRE) {
      $scope.error = "The Passwords are not the same";
    } else {

      var userInfo = {
          username: user.email,
          password: user.password,
          firstName: user.firstName,
          lastName: user.lastName
        }

      /*   Check if email is already registered     */
      $http.post('http://joshhughes.co/wp-json/cave_func/v1/email_validate', userInfo
      ).then(function successCallback(response) {
      console.log(response);
      if (response.data == "Invalid email") {
        $scope.error = response.data;
      } else if (response.data == "Email is already registered") {
        $scope.error = response.data;
      } else {
        console.log(response.data);
        
      }

  }, function errorCallback(response) {
    console.log('Error!');
      console.log(response);
  });
      

        
        console.log(userInfo);
    }
  };

  $scope.goToLogin = function() {
    $state.go('login');
  };

  $scope.requestNewPassword = function() {
    console.log("requesting new password");
  };

})

// TAB CONTROLLERS

.controller('DashCtrl', function($scope , $http, $state, $ionicModal, AuthService) {

  $scope.loading = false;

  $scope.isLoggedIn = function() {

    if (AuthService.getUser() == null || AuthService.getUser() == 'undefined') {
      $state.go('login')
    }

  }

  $scope.posts = [];


  $scope.getCavePosts = function() {
    $http.get('http://joshhughes.co/wp-json/wp/v2/posts'
      ).then(function successCallback(response) {
      $scope.posts = response.data;
      $scope.loading = false;

  }, function errorCallback(response) {
    console.log('Error!');
      console.log(response);
      $scope.loading = false;
  }).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
     });
  }

  $scope.initialGetCavePosts = function() {
    $scope.loading = true;
    $http.get('http://joshhughes.co/wp-json/wp/v2/posts'
      ).then(function successCallback(response) {
      $scope.posts = response.data;
      $scope.loading = false;

  }, function errorCallback(response) {
    console.log('Error!');
      console.log(response);
      $scope.loading = false;
  }).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
     });
  }

  $scope.user = '';
  $scope.getUserName = function() {
    $scope.user = AuthService.getUser();
  }

  $scope.isLoggedIn();
  $scope.getUserName();
  $scope.initialGetCavePosts();


})

.controller('CreateCtrl', function($scope , $http, AuthService ) {
  
  $scope.showPost = false;
  $scope.showHotspot = false;

  $scope.user = AuthService.getUser();

  $scope.createPost = function() {
    $scope.showPost = true;
    $scope.showHotspot = false;
  }

  $scope.createHotspot = function() {
    $scope.showHotspot = true;
    $scope.showPost = false;
  }

  $scope.uploadPost = function(post) {

     var postContent = {};
     postContent.content = post.info;
     postContent.title = Date.now();
     postContent.excerpt = post.info;
     postContent.status = "publish";

     
    console.log(postContent);


    $http.post('http://joshhughes.co/wp-json/wp/v2/posts', postContent).then(function successCallback(response) {
      console.log('Success!');
      console.log(response);
      $scope.closeModal();

    }, function errorCallback(response) {
      console.log('Error!');
        console.log(response);
        $scope.error_mess = response.data.message;
      });
  }

  $scope.address = '';
  $scope.spotdesc = '';
  $scope.spottitle = '';
  $scope.spotdate = '';
  $scope.lat = '';
  $scope.lng = '';
  

  $scope.uploadSpot = function(spot) {

    var Geocoder = new google.maps.Geocoder();

    $scope.address = spot.address;
    $scope.spotdesc = spot.desc;
    $scope.spottitle = spot.title;
    $scope.spotdate = spot.date;
    $scope.spottime = spot.time;
  
    Geocoder.geocode({'address' : $scope.address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      $scope.lat = results[0].geometry.location.lat();
      $scope.lng = results[0].geometry.location.lng();
      console.log($scope.lat + ' ' + $scope.lng + ' ' + $scope.spottitle + ' ' + $scope.spotdate + ' ' + $scope.spotdesc + ' ' + $scope.spottime);
      var spotContent = {};
      spotContent.cave_spot_title = $scope.spottitle;
      spotContent.cave_spot_date = $scope.spotdate;
      spotContent.cave_spot_time = $scope.spottime;
      spotContent.cave_spot_desc = $scope.spotdesc;
      spotContent.cave_spot_address = $scope.address;
      spotContent.cave_spot_lat = $scope.lat;
      spotContent.cave_spot_lng = $scope.lng;

      var spot = {};
      spot.title = $scope.spottitle;
      spot.status = 'publish';
      spot.excerpt = $scope.address;
      spot.content = $scope.address + ' ' + Date.now();

      $http.post('http://joshhughes.co/wp-json/wp/v2/hotspots', spot).then(function successCallback(response) {
      console.log('Success!');
      console.log(response);
      var spotFinalInfo = {};
      spotFinalInfo.spotID = response.data.id;
      spotFinalInfo.spotMeta = spotContent;

            $http.post('http://joshhughes.co/wp-json/cave_func/v1/hotspots_upload', spotFinalInfo).then(function successCallback(response) {
            console.log('Success!');
            console.log(response);
            

          }, function errorCallback(response) {
            console.log('Error!');
              console.log(response);
              $scope.error_mess = response.data.message;
            });

    }, function errorCallback(response) {
      console.log('Error!');
        console.log(response);
        $scope.error_mess = response.data.message;
      });

    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });

  }
  
})

.controller('HotspotsCtrl', function($scope , $http , $stateParams, AuthService) {

  $scope.loading = false;

  $scope.isLoggedIn = function() {

    if (AuthService.getUser() == null || AuthService.getUser() == 'undefined') {
      $state.go('login')
    }

  }
  

   $scope.map = '';

  $scope.hotspots = [];

  $scope.getHotspots = function() {

    $http.get('http://joshhughes.co/wp-json/cave_func/v1/hotspots'
      ).then(function successCallback(response) {
        $scope.hotspots = response.data;
        $scope.loading = false;

    }, function errorCallback(response) {
      console.log('Error!');
        console.log(response);
        $scope.loading = false;
    }).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
     });
  }
  $scope.initialGetHotspots = function() {
    $scope.loading = true;

    $http.get('http://joshhughes.co/wp-json/cave_func/v1/hotspots'
      ).then(function successCallback(response) {
        $scope.hotspots = response.data;
        $scope.loading = false;

    }, function errorCallback(response) {
      console.log('Error!');
        console.log(response);
        $scope.loading = false;
    }).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
     });
  }
  $scope.isLoggedIn();
  $scope.initialGetHotspots();

 

})

.controller('HotspotsDetailCtrl', function($scope , $http , $stateParams) {
  $scope.spottitle = $stateParams.spottitle;
  $scope.lat = $stateParams.lat;
  $scope.lng = $stateParams.lng;
  $scope.time = $stateParams.time;
  $scope.desc = $stateParams.desc;
  $scope.date = $stateParams.date;
  $scope.attending = $stateParams.attending;
  console.log($scope.spottitle);

  $scope.map = null;
  $scope.marker = null;
  $scope.myLatLng = null;

  $scope.initMap = function() {
    $scope.myLatLng = {lat: parseFloat($scope.lat), lng: parseFloat($scope.lng)};

    $scope.map = new google.maps.Map(document.getElementById('map'), {
      center: $scope.myLatLng,
      zoom: 13
    });
    $scope.maker = new google.maps.Marker({
      position: $scope.myLatLng,
      map: $scope.map,
      title: $scope.name
    });

   }

   $scope.initMap();
  
})

.controller('EventsCtrl', function($scope , $http, AuthService) {

  $scope.loading = false;
  
  $scope.calendar = {};

  $scope.isLoggedIn = function() {

    if (AuthService.getUser() == null || AuthService.getUser() == 'undefined') {
      $state.go('login')
    }

  }

  $scope.getDate = function(event) {

        if(!event.allday && !event.multidays){
            var d = moment(event.date_start).format("MMMM DD, YYYY");
            var st = moment(event.date_start).format("hh:mma");
            var et = moment(event.date_end).format("hh:mma");
             // return "Stuff";
             return d + ' | ' + st + ' - ' + et;
        } else {

            if(event.multidays > 1){
                sd = moment(event.date_start).format("DD");
                ed = moment(event.date_end).format("DD");

                sm = moment(event.date_start).format("MMMM");
                em = moment(event.date_end).format("MMMM");
                
                sy = moment(event.date_start).format("YYYY");
                ey = moment(event.date_end).format("YYYY");

                if(sy != ey){

                    return sm + " " + sd + ", " + sy + " - " + em + " " + ed + ", " + ey;

                } else if(sm != em){

                    return sm + " " + sd + " - " + em + " " + ed + ", " + ey;

                } else {
                    
                    return sm + " " + sd + "-" + ed + ", " + ey;

                }

            } else {
                return moment(event.date_start).format("MMMM DD, YYYY");
            }

            // if(event.allday){
            // } else {
            //     return moment(event.date_start).format("MMMM DD, YYYY | hh:mma");
            // }
        }
  }

  $scope.getCaveCalendar = function() {
    $http.get('http://joshhughes.co/wp-json/cave_func/v1/calendar'
      ).then(function successCallback(response) {
          $scope.calendar = response.data;
          console.log($scope.calendar);
          $scope.loading = false;
      }, function errorCallback(response) {
          console.log('Error!');
          console.log(response);
          $scope.loading = false;
      }).finally(function() {
       // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
     });
  }

  $scope.initialGetCaveCalendar = function() {
    $scope.loading = true;
    $http.get('http://joshhughes.co/wp-json/cave_func/v1/calendar'
      ).then(function successCallback(response) {
          $scope.calendar = response.data;
          console.log($scope.calendar);
          $scope.loading = false;
      }, function errorCallback(response) {
          console.log('Error!');
          console.log(response);
          $scope.loading = false;
      }).finally(function() {
       // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
     });
  }

  // if ($scope.loaded == false) {
  //   angular.element('.loading').show();
  // } else {
  //   angular.element('.loading').hide();
  // }
  

  $scope.initialGetCaveCalendar();
  $scope.isLoggedIn();
})

.controller('AccountCtrl', function($scope, AuthService, $state, $http) {
  $scope.isLoggedIn = function() {

    if (AuthService.getUser() == null || AuthService.getUser() == 'undefined') {
      $state.go('login')
    }

  }
  $scope.currentUserID = AuthService.getUser();

  $scope.userPosts = {};

  $scope.getUserPosts = function() {

    $http.get('http://joshhughes.co/wp-json/wp/v2/posts?filter[author]=' + $scope.currentUserID.user_id
      ).then(function successCallback(response) {
        console.log(response.data)
      $scope.userPosts = response.data;

  }, function errorCallback(response) {
    console.log('Error!');
      console.log(response);
  }).finally(function() {
       // Stop the ion-refresher from spinning
       $scope.$broadcast('scroll.refreshComplete');
     });

  }

  $scope.deleteShow = false;
  $scope.currentPostID = '';

  $scope.revealDelete = function(postInfo) {

    $scope.currentPostID = postInfo;
    $scope.deleteShow = true;
    console.log($scope.currentPostID);
    var q = window.confirm("Do you want to delete this post?");
    if (q == true) {
      window.alert("The post will be deleted.");

      $http.post('http://joshhughes.co/wp-json/cave_func/v1/post_delete', $scope.currentPostID
      ).then(function successCallback(response) {
        console.log(response.data)

  }, function errorCallback(response) {
    console.log('Error!');
      console.log(response);
  })


    }
  };

  $scope.settings = {
    enableFriends: true
  };

  $scope.logOut = function() {
    AuthService.removeUser();
    $state.go('login');
  }
  $scope.user = AuthService.getUser();

  $scope.isLoggedIn();
  $scope.getUserPosts();

});
